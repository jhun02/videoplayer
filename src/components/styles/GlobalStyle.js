import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
   
  }
  html {
     background: #333;
  }
  body {
    font-size: 16px;
    color: red;
    font-family: 'Hind', sans-serif;
  }
`;

export default GlobalStyle;
