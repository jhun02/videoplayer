import React from "react";

const Notfound = () => <p>Page not Found!</p>;

export default Notfound;
