import React from "react";
import WbnPlayer from "./WbnPlayer";
import Notfound from "./Notfound";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import GlolbalStyle from "../styles/GlobalStyle";

const App = () => (
  <BrowserRouter>
    <>
      <Switch>
        <Route exact path="/" component={WbnPlayer} />
        <Route exact path="/:activeVideo" component={WbnPlayer} />
        <Route component={Notfound} />
      </Switch>
      <GlolbalStyle />
    </>
  </BrowserRouter>
);

export default App;
